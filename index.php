<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercices d'introduction php</title>
</head>
<body>

<h1>Exercice 1</h1>
<!-- Créer une variable et l'initialiser à 0. Tant que cette variable n'atteint pas 10 :

    l'afficher
    incrémenter de 1 -->

<?php

$i = 0;

while($i < 10) {

    echo $i++;
}

?>

<h1>Exercice 2</h1>
<!-- Créer deux variables. Initialiser la première à 0 et la deuxième avec un nombre compris en 1 et 100. Tant que la première variable n'est pas supérieur à 20 :

    multiplier la première variable avec la deuxième
    afficher le résultat
    incrémenter la première variable -->


<?php

$i=0;
$b=70;

while ($i < 20 ){

   echo $i*$b;
   echo "<br />";
$i++;

}

?>

<h1>Exercice 3</h1>
<!-- Créer deux variables. Initialiser la première à 100 et la deuxième avec un nombre compris en 1 et 100. Tant que la première variable n'est pas inférieur ou égale à 10 :

multiplier la première variable avec la deuxième
afficher le résultat
décrémenter la première variable -->


<?php
$c=100;
$m=45;

while (!($c <= 10)){

    echo $c*$m;
    $c--;
    echo "<br />";
}

?>

<h1>Exercice 4</h1>
<!-- Créer une variable et l'initialiser à 1. Tant que cette variable n'atteint pas 10 :

    l'afficher
    l'incrementer de la moitié de sa valeur -->

<?php

$a=1;
while ( $a <= 10){
    echo $a;
    echo "<br />";
    $a = $a + ($a/2);

}

?>

<h1>Exercice 5</h1>
<!-- En allant de 1 à 15 avec un pas de 1, afficher le message On y arrive presque... -->
<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


$p=0;

while( $p <= 15 ){

    echo $p;
    $p++;
    echo "On y arrive presque...";
    echo "<br />";
}

?>


<h1>Exercice 6 </h1>
<!-- En allant de 20 à 0 avec un pas de 1, afficher le message C'est presque bon... -->

<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


$p=20;

while( $p > 0 ){

    echo $p;
    $p--;
    echo "C'est presque bon...";
    echo "<br />";
}

?>


<h1>Exercice 7</h1>
<!-- En allant de 1 à 100 avec un pas de 15, afficher le message On tient le bon bout... -->

<?php
$p=0;

while( $p <= 100 ){

    echo $p;
    $p+=15;
    echo "On y arrive presque...";
    echo "<br />";
}

?>

<h1>Exercice 8 </h1>
<!-- En allant de 200 à 0 avec un pas de 12, afficher le message Enfin ! ! ! -->

<?php

$p=200;

while( $p > 0 ){

    echo $p;
    $p-=12;
    echo "Enfin ! ! !";
    echo "<br />";
}

?>

</body>
</html>